-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2023 at 07:55 PM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kevinstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `admin_telp` varchar(20) NOT NULL,
  `admin_email` varchar(50) NOT NULL,
  `admin_address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`admin_id`, `admin_name`, `username`, `password`, `admin_telp`, `admin_email`, `admin_address`) VALUES
(1, 'Kevin Farell Wilianto', 'kevinanaksholeh', 'login', '082137783251', '111202214754@mhs.dinus.ac.id', 'Jalan Suka Ngoding No. 02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_category`
--

CREATE TABLE `tb_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_category`
--

INSERT INTO `tb_category` (`category_id`, `category_name`) VALUES
(17, 'Sweater Unisex'),
(19, 'Sepatu Unisex'),
(25, 'Kemeja Wanita'),
(26, 'Kemeja Pria'),
(28, 'Celana Wanita'),
(29, 'Celana Pria');

-- --------------------------------------------------------

--
-- Table structure for table `tb_product`
--

CREATE TABLE `tb_product` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_price` int(11) NOT NULL,
  `product_description` text NOT NULL,
  `product_image` varchar(100) NOT NULL,
  `product_status` tinyint(1) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_product`
--

INSERT INTO `tb_product` (`product_id`, `category_id`, `product_name`, `product_price`, `product_description`, `product_image`, `product_status`, `date_created`) VALUES
(7, 26, 'Kemeja Pria Hitam H&M ', 400000, '<p>Kemeja pria dengan warna hitam yang elegan dan menggunakan bahan cotton 100%, serta warna di kemeja yang tidak mudah luntur dan awet. Semua produk yang dijual di toko kami pastinya telah lulus uji keaslian pada produk dan kondisi fisik pada produk oleh staff ahli kami..</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth it Since 2012</p>\r\n', 'produk1682166846.jpg', 1, '2023-04-22 12:34:06'),
(8, 26, 'Kemeja Pria Putih H&M ', 390000, '<p>Kemeja pria dengan warna putih dengan warna yang elegan, nyaman&nbsp;ketika dipakai dan menggunakan bahan cotton 100%, serta warna di kemeja yang tidak mudah luntur dan awet. Semua produk yang dijual di toko kami pastinya telah lulus uji keaslian pada produk dan kondisi fisik pada produk oleh staff ahli kami..</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth it Since 2012</p>\r\n', 'produk1682167194.jpg', 1, '2023-04-22 12:39:54'),
(9, 26, 'Kemeja Pria Merah H&M ', 450000, '<p>Kemeja pria dengan warna merah maroon&nbsp;yang elegan, warna yang tidak terlalu mencolok, dan menggunakan bahan cotton 100% yang pastinya nyaman dipakai, serta warna di kemeja yang tidak mudah luntur dan awet. Semua produk yang dijual di toko kami pastinya telah lulus uji keaslian pada produk dan kondisi fisik pada produk oleh staff ahli kami..</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth it Since 2012</p>\r\n', 'produk1682167736.jpg', 1, '2023-04-22 12:48:56'),
(10, 29, 'Celana Pria Hitam Uniqlo', 399999, '<p>Celana pria dengan warna hitam&nbsp;yang elegan,&nbsp;dan menggunakan bahan chino&nbsp;yang pastinya nyaman dipakai, serta warna di celana&nbsp;yang tidak mudah luntur dan awet. Semua produk yang dijual di toko kami pastinya telah lulus uji keaslian pada produk dan kondisi fisik pada produk oleh staff ahli kami.</p>\r\n\r\n<p>Bahan yang digunakan :&nbsp;</p>\r\n\r\n<p>Refined Polyester<br />\r\nTerbuat dari benang polyester level tertinggi dan di anyam dengan metode locked-twill. Ini adalah generasi teratas dari celana super nyaman yang ada di brand Indonesia dan di olah khusus oleh tim kami.<br />\r\n- Cool Breathable Fabric<br />\r\nTerdapat sensasi dingin ketika menggunakan celana ini karena teknologi serat yang digunakan mampu bernadas sehingga kaki kamu tidak perl merasa kegerahan lagi<br />\r\n- Lightweight 240 GSM<br />\r\nMasuk kategori celana ringan namun solid dengan hadirnya<br />\r\nCenterFold&trade; yaitu lipatan tengah di bagian depan celana yang tetap terjaga ketika sampai di tempat pembeli<br />\r\n- Tailored Side Pocket<br />\r\nBagian kantong samping dibuat tertutup dengan kualitas jahitan tailor khas Kasual<br />\r\n- Smart Stitching<br />\r\nKasual menggunakan metode jahitan pintar dimana kualitas tiap ketukan jahit tetap lurus dan rapih. Ini bisa dilihat baik di bagian luar maupun dalam celana<br />\r\n- Quick Dry &amp; Wrinkle Free<br />\r\nAnkle Prime anti leek dan apabila terkena air maupun setelah di cuci bisa kering&nbsp;dengan&nbsp;cepat</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth it Since 2012</p>\r\n', 'produk1682168194.jpg', 1, '2023-04-22 12:56:34'),
(11, 29, 'Celana Pria Khaki Jogger Cargo Pants Uniqlo', 499999, '<p>Celana jogger&nbsp;dengan warna khaki&nbsp;yang elegan,&nbsp;dan menggunakan bahan jogger yang pastinya nyaman dipakai, serta warna di celana&nbsp;yang tidak mudah luntur dan awet. Semua produk yang dijual di toko kami pastinya telah lulus uji keaslian pada produk dan kondisi fisik pada produk oleh staff ahli kami.</p>\r\n\r\n<p>Bahan yang digunakan :&nbsp;</p>\r\n\r\n<p>Refined Polyester<br />\r\nTerbuat dari benang polyester level tertinggi dan di anyam dengan metode locked-twill. Ini adalah generasi teratas dari celana super nyaman yang ada di brand Indonesia dan di olah khusus oleh tim kami.<br />\r\n- Cool Breathable Fabric<br />\r\nTerdapat sensasi dingin ketika menggunakan celana ini karena teknologi serat yang digunakan mampu bernadas sehingga kaki kamu tidak perl merasa kegerahan lagi<br />\r\n- Lightweight 240 GSM<br />\r\nMasuk kategori celana ringan namun solid dengan hadirnya<br />\r\nCenterFold&trade; yaitu lipatan tengah di bagian depan celana yang tetap terjaga ketika sampai di tempat pembeli<br />\r\n- Tailored Side Pocket<br />\r\nBagian kantong samping dibuat tertutup dengan kualitas jahitan tailor khas Kasual<br />\r\n- Smart Stitching<br />\r\nKasual menggunakan metode jahitan pintar dimana kualitas tiap ketukan jahit tetap lurus dan rapih. Ini bisa dilihat baik di bagian luar maupun dalam celana<br />\r\n- Quick Dry &amp; Wrinkle Free<br />\r\nAnkle Prime anti leek dan apabila terkena air maupun setelah di cuci bisa kering&nbsp;dengan&nbsp;cepat</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth it Since 2012</p>\r\n', 'produk1682168466.jpg', 1, '2023-04-22 13:01:06'),
(12, 29, 'Celana Jeans Pria Uniqlo', 699999, '<p>Celana jeans dengan warna biru tua yang elegan,&nbsp;yang pastinya nyaman dipakai, serta warna di celana&nbsp;yang tidak mudah luntur dan awet. Semua produk yang dijual di toko kami pastinya telah lulus uji keaslian pada produk dan kondisi fisik pada produk oleh staff ahli kami.</p>\r\n\r\n<p>Bahan yang digunakan :&nbsp;</p>\r\n\r\n<p>Refined Polyester<br />\r\nTerbuat dari benang polyester level tertinggi dan di anyam dengan metode locked-twill. Ini adalah generasi teratas dari celana super nyaman yang ada di brand Indonesia dan di olah khusus oleh tim kami.<br />\r\n- Cool Breathable Fabric<br />\r\nTerdapat sensasi dingin ketika menggunakan celana ini karena teknologi serat yang digunakan mampu bernadas sehingga kaki kamu tidak perl merasa kegerahan lagi<br />\r\n- Lightweight 240 GSM<br />\r\nMasuk kategori celana ringan namun solid dengan hadirnya<br />\r\nCenterFold&trade; yaitu lipatan tengah di bagian depan celana yang tetap terjaga ketika sampai di tempat pembeli<br />\r\n- Tailored Side Pocket<br />\r\nBagian kantong samping dibuat tertutup dengan kualitas jahitan tailor khas Kasual<br />\r\n- Smart Stitching<br />\r\nKasual menggunakan metode jahitan pintar dimana kualitas tiap ketukan jahit tetap lurus dan rapih. Ini bisa dilihat baik di bagian luar maupun dalam celana<br />\r\n- Quick Dry &amp; Wrinkle Free<br />\r\nAnkle Prime anti leek dan apabila terkena air maupun setelah di cuci bisa kering&nbsp;dengan&nbsp;cepat</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth it Since 2012</p>\r\n', 'produk1682181266.jpg', 1, '2023-04-22 13:05:34'),
(13, 28, 'Celana Baggy Wanita Chinos Uniqlo', 459000, '<p>Celana Baggy Chinos hadir kembali! Didesain khusus jatuh persis di pergelangan kaki agar menciptakan tampilan semampai.<br />\r\nterdapat YKK zipper, hook, safety button, belt loop, dengan dua saku di sisi kanan kiri dan dua saku di sisi belakang.<br />\r\nKhusus warna Broken White, disarankan untuk pilih satu size di atas yang biasanya kamu pakai agar menghindari look yang terlalu ketat atau mencetak bentuk tubuh saat dipakai.<br />\r\nMaterial:<br />\r\nNo-wrinkle Cotton<br />\r\nDengan paduan serat katun dan polyester, sehingga jatuh secara sempurna, tidak kaku dan travel friendly karena tidak mudah kusut.<br />\r\nInfo:<br />\r\n1. Harap ukur sesuai detail size yang sudah diberikan<br />\r\n2. Tinggi badan model 168 cm<br />\r\n3. On model menggunakan size M<br />\r\nPlease note that actual color may slightly different from your&nbsp;display&nbsp;screen</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth It Since 2012</p>\r\n', 'produk1682169141.jpg', 1, '2023-04-22 13:12:21'),
(14, 28, 'Celana Wanita Cooper Denim', 559999, '<p>Celana Baggy Chinos hadir kembali! Didesain khusus jatuh persis di pergelangan kaki agar menciptakan tampilan semampai.<br />\r\nterdapat YKK zipper, hook, safety button, belt loop, dengan dua saku di sisi kanan kiri dan dua saku di sisi belakang.<br />\r\nKhusus warna Broken White, disarankan untuk pilih satu size di atas yang biasanya kamu pakai agar menghindari look yang terlalu ketat atau mencetak bentuk tubuh saat dipakai.<br />\r\nMaterial:<br />\r\nNo-wrinkle Cotton<br />\r\nDengan paduan serat katun dan polyester, sehingga jatuh secara sempurna, tidak kaku dan travel friendly karena tidak mudah kusut.<br />\r\nInfo:<br />\r\n1. Harap ukur sesuai detail size yang sudah diberikan<br />\r\n2. Tinggi badan model 168 cm<br />\r\n3. On model menggunakan size M<br />\r\nPlease note that actual color may slightly different from your&nbsp;display&nbsp;screen</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth It Since 2012</p>\r\n', 'produk1682169307.jpg', 1, '2023-04-22 13:15:07'),
(15, 28, 'Celana Button Pants Wanita', 449999, '<p>Celana Baggy Chinos hadir kembali! Didesain khusus jatuh persis di pergelangan kaki agar menciptakan tampilan semampai.<br />\r\nterdapat YKK zipper, hook, safety button, belt loop, dengan dua saku di sisi kanan kiri dan dua saku di sisi belakang.<br />\r\nKhusus warna Broken White, disarankan untuk pilih satu size di atas yang biasanya kamu pakai agar menghindari look yang terlalu ketat atau mencetak bentuk tubuh saat dipakai.<br />\r\nMaterial:<br />\r\nNo-wrinkle Cotton<br />\r\nDengan paduan serat katun dan polyester, sehingga jatuh secara sempurna, tidak kaku dan travel friendly karena tidak mudah kusut.<br />\r\nInfo:<br />\r\n1. Harap ukur sesuai detail size yang sudah diberikan<br />\r\n2. Tinggi badan model 168 cm<br />\r\n3. On model menggunakan size M<br />\r\nPlease note that actual color may slightly different from your&nbsp;display&nbsp;screen</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth It Since 2012</p>\r\n', 'produk1682181198.jpg', 1, '2023-04-22 13:17:16'),
(16, 25, 'Kemeja Wanita Hitam H&M', 399999, '<p>Kemeja wanita&nbsp;dengan warna hitam&nbsp;dengan warna yang elegan, nyaman&nbsp;ketika dipakai dan menggunakan bahan cotton 100%, serta warna di kemeja yang tidak mudah luntur dan awet. Semua produk yang dijual di toko kami pastinya telah lulus uji keaslian pada produk dan kondisi fisik pada produk oleh staff ahli kami..</p>\r\n\r\n<p>Product Detail:<br />\r\nBahan katun<br />\r\nShort sleeve<br />\r\nV-Neck<br />\r\nCasual design<br />\r\nSoft material<br />\r\nComfortable</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth it Since 2012</p>\r\n', 'produk1682181362.jpg', 1, '2023-04-22 13:22:23'),
(17, 25, 'Kemeja Wanita Putih H&M', 459000, '<p>Kemeja wanita&nbsp;dengan warna putih dengan warna yang elegan, nyaman&nbsp;ketika dipakai dan menggunakan bahan cotton 100%, serta warna di kemeja yang tidak mudah luntur dan awet. Semua produk yang dijual di toko kami pastinya telah lulus uji keaslian pada produk dan kondisi fisik pada produk oleh staff ahli kami..</p>\r\n\r\n<p>Product Detail:<br />\r\nBahan katun<br />\r\nShort sleeve<br />\r\nV-Neck<br />\r\nCasual design<br />\r\nSoft material<br />\r\nComfortable</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth it Since 2012</p>\r\n', 'produk1682181135.jpg', 1, '2023-04-22 13:25:31'),
(18, 25, 'Kemeja Wanita Merah H&M', 359000, '<p>Kemeja wanita&nbsp;dengan warna merah&nbsp;dengan warna yang elegan, nyaman&nbsp;ketika dipakai dan menggunakan bahan cotton 100%, serta warna di kemeja yang tidak mudah luntur dan awet. Semua produk yang dijual di toko kami pastinya telah lulus uji keaslian pada produk dan kondisi fisik pada produk oleh staff ahli kami..</p>\r\n\r\n<p>Product Detail:<br />\r\nBahan katun<br />\r\nShort sleeve<br />\r\nV-Neck<br />\r\nCasual design<br />\r\nSoft material<br />\r\nComfortable</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth it Since 2012</p>\r\n', 'produk1682170246.jpg', 1, '2023-04-22 13:30:46'),
(19, 19, 'Sepatu Air Jordan 1 Low Washed Denim (GS)', 2799000, '<p>Brand New In Box<br />\r\nSize: Ask<br />\r\n(cantumkan ukuran yang dipesan dinotes)<br />\r\nHarga dapat berubah sewaktu-waktu, sesuai dengan ukuran<br />\r\nvang di pesan.<br />\r\n*Penukaran ukuran di perbolehkan, selama ketersediaan<br />\r\nukuran&nbsp;masih&nbsp;ada.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>100% Original Authentic<br />\r\n100% Trusted and Worth It Since 2012</p>\r\n', 'produk1682193159.jpg', 1, '2023-04-22 13:36:31'),
(20, 19, 'Sepatu Air Jordan 1 Low Washed Denim', 2399000, '<p>Brand New In Box<br />\r\nSize: Ask<br />\r\n(cantumkan ukuran yang dipesan dinotes)<br />\r\nHarga dapat berubah sewaktu-waktu, sesuai dengan ukuran<br />\r\nvang di pesan.<br />\r\n*Penukaran ukuran di perbolehkan, selama ketersediaan<br />\r\nukuran&nbsp;masih&nbsp;ada.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>100% Original Authentic<br />\r\n100% Trusted and Worth It Since 2012</p>\r\n', 'produk1682170750.jpg', 1, '2023-04-22 13:38:35'),
(21, 19, 'Sepatu Air Jordan 1 Low Smoke Grey', 2399000, '<p>Brand New In Box<br />\r\nSize: Ask<br />\r\n(cantumkan ukuran yang dipesan dinotes)<br />\r\nHarga dapat berubah sewaktu-waktu, sesuai dengan ukuran<br />\r\nvang di pesan.<br />\r\n*Penukaran ukuran di perbolehkan, selama ketersediaan<br />\r\nukuran&nbsp;masih&nbsp;ada.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>100% Original Authentic<br />\r\n100% Trusted and Worth It Since 2012</p>\r\n', 'produk1682170902.jpg', 1, '2023-04-22 13:41:42'),
(22, 19, 'Sepatu Air Jordan 1 Low SE Tokyo Vintage UNC Grey', 3299000, '<p>Brand New In Box<br />\r\nSize: Ask<br />\r\n(cantumkan ukuran yang dipesan dinotes)<br />\r\nHarga dapat berubah sewaktu-waktu, sesuai dengan ukuran<br />\r\nvang di pesan.<br />\r\n*Penukaran ukuran di perbolehkan, selama ketersediaan<br />\r\nukuran&nbsp;masih&nbsp;ada.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>100% Original Authentic<br />\r\n100% Trusted and Worth It Since 2012</p>\r\n', 'produk1682171028.jpg', 1, '2023-04-22 13:43:48'),
(23, 19, 'Sepatu Air Jordan 1 Mid SE DIY Doodle GS', 1999000, '<p>Brand New In Box<br />\r\nSize: Ask<br />\r\n(cantumkan ukuran yang dipesan dinotes)<br />\r\nHarga dapat berubah sewaktu-waktu, sesuai dengan ukuran<br />\r\nvang di pesan.<br />\r\n*Penukaran ukuran di perbolehkan, selama ketersediaan<br />\r\nukuran&nbsp;masih&nbsp;ada.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>100% Original Authentic<br />\r\n100% Trusted and Worth It Since 2012</p>\r\n', 'produk1682171200.jpg', 1, '2023-04-22 13:46:40'),
(24, 19, 'Sepatu Air Jordan 1 Low Travis Scott X Fragment', 31990000, '<p>Brand New In Box<br />\r\nSize: Ask<br />\r\n(cantumkan ukuran yang dipesan dinotes)<br />\r\nHarga dapat berubah sewaktu-waktu, sesuai dengan ukuran<br />\r\nvang di pesan.<br />\r\n*Penukaran ukuran di perbolehkan, selama ketersediaan<br />\r\nukuran&nbsp;masih&nbsp;ada.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>100% Original Authentic<br />\r\n100% Trusted and Worth It Since 2012</p>\r\n', 'produk1682171403.jpg', 1, '2023-04-22 13:50:03'),
(25, 17, 'Sweater Unisex PlayStation H&M', 399000, '<p>- BRAND NEW<br />\r\n- Full Tag ( Tag Merk, Tag Price, and Tag Wash)<br />\r\n- UNISEX ( PRIA DAN WANITA)<br />\r\n- UKURAN TERSEDIA S/M/L/XL<br />\r\n&bull; Size Chart:<br />\r\n- S = P 62 XL 50 LD 100<br />\r\n- M = P 65 X L 55 LD 110<br />\r\n- L = P 68 X L 58 LD 116<br />\r\n- XL = P 70 X L 60 LD 120<br />\r\n(KET: P = Panjang, L = Lebar, LD = Lingkar Dada)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth It Since 2012</p>\r\n', 'produk1682178933.jpg', 1, '2023-04-22 13:56:09'),
(26, 17, 'Sweater Unisex Marvel H&M', 399999, '<p>- BRAND NEW<br />\r\n- Full Tag ( Tag Merk, Tag Price, and Tag Wash)<br />\r\n- UNISEX ( PRIA DAN WANITA)<br />\r\n- UKURAN TERSEDIA S/M/L/XL<br />\r\n&bull; Size Chart:<br />\r\n- S = P 62 XL 50 LD 100<br />\r\n- M = P 65 X L 55 LD 110<br />\r\n- L = P 68 X L 58 LD 116<br />\r\n- XL = P 70 X L 60 LD 120<br />\r\n(KET: P = Panjang, L = Lebar, LD = Lingkar Dada)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth It Since 2012</p>\r\n', 'produk1682171865.jpg', 1, '2023-04-22 13:57:45'),
(27, 17, 'Sweater Unisex Garfield H&M', 399999, '<p>- BRAND NEW<br />\r\n- Full Tag ( Tag Merk, Tag Price, and Tag Wash)<br />\r\n- UNISEX ( PRIA DAN WANITA)<br />\r\n- UKURAN TERSEDIA S/M/L/XL<br />\r\n&bull; Size Chart:<br />\r\n- S = P 62 XL 50 LD 100<br />\r\n- M = P 65 X L 55 LD 110<br />\r\n- L = P 68 X L 58 LD 116<br />\r\n- XL = P 70 X L 60 LD 120<br />\r\n(KET: P = Panjang, L = Lebar, LD = Lingkar Dada)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth It Since 2012</p>\r\n', 'produk1682171983.jpg', 1, '2023-04-22 13:59:43'),
(28, 17, 'Sweater Unisex Nirvana H&M', 399999, '<p>- BRAND NEW<br />\r\n- Full Tag ( Tag Merk, Tag Price, and Tag Wash)<br />\r\n- UNISEX ( PRIA DAN WANITA)<br />\r\n- UKURAN TERSEDIA S/M/L/XL<br />\r\n&bull; Size Chart:<br />\r\n- S = P 62 XL 50 LD 100<br />\r\n- M = P 65 X L 55 LD 110<br />\r\n- L = P 68 X L 58 LD 116<br />\r\n- XL = P 70 X L 60 LD 120<br />\r\n(KET: P = Panjang, L = Lebar, LD = Lingkar Dada)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>100% Original Authentic</p>\r\n\r\n<p>100% Trusted and Worth It Since 2012</p>\r\n', 'produk1683136360.jpg', 1, '2023-04-22 14:05:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tb_category`
--
ALTER TABLE `tb_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `tb_product`
--
ALTER TABLE `tb_product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `category_id` (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_category`
--
ALTER TABLE `tb_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tb_product`
--
ALTER TABLE `tb_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
